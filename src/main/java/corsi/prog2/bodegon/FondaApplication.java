package corsi.prog2.bodegon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FondaApplication {

	public static void main(String[] args) {
		SpringApplication.run(FondaApplication.class, args);
	}

}
