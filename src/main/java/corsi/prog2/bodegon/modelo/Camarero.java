package corsi.prog2.bodegon.modelo;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "Camarero")
@DiscriminatorValue("Camarero")
public class Camarero extends Usuario {

    public Camarero() {
    }

    public Camarero(String codigo, String nombre, String password) {
        super(codigo, nombre, password);
    }
    
}
