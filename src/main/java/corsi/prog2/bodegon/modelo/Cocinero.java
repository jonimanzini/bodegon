package corsi.prog2.bodegon.modelo;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "Cocinero")
@DiscriminatorValue("Cocinero")
public class Cocinero extends Usuario {

    public Cocinero() {
    }

    public Cocinero(String codigo, String nombre, String password) {
        super(codigo, nombre, password);
    }
    
}
