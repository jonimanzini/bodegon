package corsi.prog2.bodegon.modelo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioDeComandas extends JpaRepository<Comanda, Long> {

    public List<Comanda> findByCamarero(Usuario camarero);

    public List<Comanda> findByMesa(Integer mesa);

    public List<Comanda> findByLista(boolean lista);
    
    public List<Comanda> findByCamareroAndLista(Usuario camarero, boolean lista);
    
    public Long countByCamarero(Usuario camarero);

    public Long countByItems(Item item);
    
}
