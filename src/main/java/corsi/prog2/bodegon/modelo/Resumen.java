package corsi.prog2.bodegon.modelo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Resumen {

    private Double recaudacion;
    private Preparacion preparacionMasPedida;
    private Bebida bebidaMasPedida;
    private Camarero camareroMasPedidos;

    public Double getRecaudacion() {
        return recaudacion;
    }

    public void setRecaudacion(Double recaudacion) {
        this.recaudacion = recaudacion;
    }

    public Preparacion getPreparacionMasPedida() {
        return preparacionMasPedida;
    }

    public void setPreparacionMasPedida(Preparacion preparacionMasPedida) {
        this.preparacionMasPedida = preparacionMasPedida;
    }

    public Bebida getBebidaMasPedida() {
        return bebidaMasPedida;
    }

    public void setBebidaMasPedida(Bebida bebidaMasPedida) {
        this.bebidaMasPedida = bebidaMasPedida;
    }

    public Camarero getCamareroMasPedidos() {
        return camareroMasPedidos;
    }

    public void setCamareroMasPedidos(Camarero camareroMasPedidos) {
        this.camareroMasPedidos = camareroMasPedidos;
    }
    
    public void finalizarJornada(List<Comanda> comandas) {
        double recaudacion = 0;
        Map<Usuario, Integer> atencion = new HashMap<>();
        Map<Item, Integer> bebidas = new HashMap<>();
        Map<Item, Integer> preparaciones = new HashMap<>();

        for (Comanda c : comandas) {
            if (atencion.containsKey(c.getCamarero())) {
                atencion.replace(c.getCamarero(), atencion.get(c.getCamarero()) + 1);
            } else {
                atencion.put(c.getCamarero(), 1);
            }
            List<Item> items = c.getItems();
            for (Item i : items) {
                Map<Item, Integer> bp = i.getTipo().equals("Bebida") ? bebidas : preparaciones;
                if (bp.containsKey(i)) {
                    bp.replace(i, bp.get(i) + 1);
                } else {
                    bp.put(i, 1);
                }
                recaudacion += i.getPrecio();
            }
        }

        setCamareroMasPedidos((Camarero) obtenerMaximo(atencion));
        setBebidaMasPedida((Bebida) obtenerMaximo(bebidas));
        setPreparacionMasPedida((Preparacion) obtenerMaximo(preparaciones));
        setRecaudacion(recaudacion);
    }

    private <K, V extends Comparable<V>> K obtenerMaximo(Map<K, V> map) {
        Map.Entry<K, V> maxEntry = null;
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (maxEntry == null
                    || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }
        return maxEntry.getKey();
    }
}
